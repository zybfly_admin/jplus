package io.jplus.admin.service.impl;

import com.jfinal.plugin.activerecord.Page;
import io.jboot.aop.annotation.Bean;
import io.jboot.components.rpc.annotation.RPCBean;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;
import io.jplus.admin.model.Log;
import io.jplus.admin.service.LogService;
import io.jplus.common.Query;


@Bean
@RPCBean
public class LogServiceImpl extends JbootServiceBase<Log> implements LogService {

    @Override
    public boolean deleteAll() {
        return false;
    }

    @Override
    public Page<Log> queryPage(Query query) {
        Columns columns = Columns.create();
        return DAO.paginateByColumns(query.getCurrPage(), query.getLimit(), columns, query.getOrderBy());
    }

    @Override
    public boolean deleteById(Object... ids) {
        boolean tag = false;
        for (Object id : ids) {
            tag = DAO.deleteById(id);
        }
        return tag;
    }
}