package io.jplus.admin.controller;

import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.POST;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jplus.JplusConsts;
import io.jplus.core.web.base.BaseController;
import org.apache.shiro.authz.annotation.RequiresPermissions;

@RequestMapping(value = "/admin/schedule", viewPath = JplusConsts.BASE_VIEW_PATH + "admin")
public class ScheduleJobController extends BaseController {
    public void index() {
        render("schedule.html");
    }

    @RequiresPermissions("sys:schedulejob:list")
    public void list(){

    }

    @RequiresPermissions("sys:schedulejob:info")
    public void info(){

    }

    @Before(POST.class)
    @RequiresPermissions("sys:schedulejob:save")
    public void save(){

    }

    @Before(POST.class)
    @RequiresPermissions("sys:schedulejob:update")
    public void update(){

    }

    @RequiresPermissions("sys:schedulejob:delete")
    public void delete(){

    }
}
