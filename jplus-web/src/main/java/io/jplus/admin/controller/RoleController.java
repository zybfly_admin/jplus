/**
 * Copyright (c) 2017-2018,Retire 吴益峰 (372310383@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jplus.admin.controller;

import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.POST;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Page;
import io.jboot.components.rpc.annotation.RPCInject;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jplus.JplusConsts;
import io.jplus.admin.model.Role;
import io.jplus.admin.service.RoleService;
import io.jplus.core.web.base.BaseController;
import org.apache.shiro.authz.annotation.RequiresPermissions;

import java.util.List;


@RequestMapping(value = "/admin/role", viewPath = JplusConsts.BASE_VIEW_PATH + "admin")
public class RoleController extends BaseController {

    @RPCInject
    RoleService roleService;

    public void index() {
        render("role.html");
    }

    @RequiresPermissions("sys:role:list")
    public void list() {
        Page<Role> page = roleService.queryPage(getQuery());
        renderJsonPage(page);
    }

    @RequiresPermissions("sys:role:info")
    public void info() {
        Role role = roleService.findRoleById(getPara());

        renderJson(Ret.ok("role", role));
    }

    @Before(POST.class)
    @RequiresPermissions("sys:role:save")
    public void save() {
        Role role = jsonToModel(Role.class);
        boolean tag = roleService.saveRole(role);
        if (tag) {
            renderJsonForSuccess();
        } else {
            renderJsonForFail("保存失败！");
        }
    }

    @Before(POST.class)
    @RequiresPermissions("sys:role:update")
    public void update() {
        Role role = jsonToModel(Role.class);
        boolean tag = roleService.updateRole(role);
        if (tag) {
            renderJsonForSuccess();
        } else {
            renderJsonForFail("更新失败！");
        }
    }

    @RequiresPermissions("sys:role:delete")
    public void delete() {
        List ids = jsonToList();
        if (ids == null) {
            renderJsonForFail("id不能为空！");
            return;
        }
        boolean tag = roleService.deleteById(ids.toArray());
        if (tag) {
            renderJsonForSuccess();
        } else {
            renderJsonForFail("删除失败！");
        }
    }


    @RequiresPermissions("sys:role:select")
    public void select() {
        List<Role> roleList = roleService.findAll();
        renderJson(Ret.ok("roleList", roleList));
    }


}
