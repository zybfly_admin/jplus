package io.jplus.admin.listener;

import com.jfinal.config.Constants;
import com.jfinal.config.Interceptors;
import com.jfinal.ext.interceptor.SessionInViewInterceptor;
import io.jboot.core.listener.JbootAppListenerBase;
import io.jboot.web.json.JbootJson;
import io.jplus.JplusConsts;
import io.jplus.admin.interceptor.JplusUserInteceptor;


/**
 * @author Retire 吴益峰 （372310383@qq.com）
 * @version V1.0
 * @Title:
 * @Package io.jplus.admin.listener
 * @create 2019-01-03 15:00
 */
public class JplusAppListener  extends JbootAppListenerBase {

    @Override
    public void onConstantConfig(Constants constants) {
        constants.setError404View(JplusConsts.BASE_VIEW_PATH + "common/404.html");
        constants.setError500View(JplusConsts.BASE_VIEW_PATH + "common/500.html");
        constants.setErrorView(401, JplusConsts.BASE_VIEW_PATH + "common/401.html");
        constants.setErrorView(403, JplusConsts.BASE_VIEW_PATH + "common/403.html");
        //使用jboot自带的JbootJson解析
        constants.setJsonFactory(()->new JbootJson());
    }

    @Override
    public void onInterceptorConfig(Interceptors interceptors) {
        interceptors.add(new SessionInViewInterceptor());
        interceptors.add(new JplusUserInteceptor());
    }

}
